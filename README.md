# gitfilters

Bash scripts to provide clean and smudge filters when checking files in and out.

## Acknowledgement

This README brought to you courtesy of the [Acme Readme Corp.](https://gist.github.com/zenorocha/4526327)

## Installation

**NB Code currently not working** - development abandoned as the logic was getting more complex than I liked for a shell script.

Development continuing using python, see [GitKeywordFilter](https://github.com/WhowantsToknow/GitKeywordFilter)

## Usage

See *Pro Git book 2nd ed. 2014* - [8.2 Git Attributes](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes) and [8.2 Git Attributes:Keyword Expansion](https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes#Keyword-Expansion)

## Reporting bugs

Please use the [GitHub issue tracker](https://github.com/WhowantsToknow/gitfilters/issues) for any bugs or feature suggestions.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

Contributions must be licensed under the GNU GPLv3.
The contributor retains the copyright.

## Credits

Ideas shamelessly cribbed from many other [GitHub bash filters](https://github.com/search?l=Shell&q=clean+smudge&type=Repositories&utf8=%E2%9C%93)

## License

gitfilters is licensed under the [GNU General Public License, v3.](LICENSE).
