#!/bin/bash
#===============================================================================
#  gitcleanfilter.sh - git clean filter written in bash to reverse keyword
#  expansion
#===============================================================================
#
#  Copyright 2015 Keith Watson <swillber@gmail.com>
#
#  This file is part of the git keyword package and is free software; you can
#  redistribute it and/or modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either version 3 of the
#  License, or (at your option) any later version.
#
#  This package is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#  details.
#
#  You should have received a copy of the GNU General Public License along with
#  this package; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#===============================================================================
#                          C O D I N G   S T Y L E
#===============================================================================
#  In general this script conforms to the Google Shell Style Guide (see:
#  http://google.github.io/styleguide/shell.xml).
#
#  Script uses CamelCaps for variable names rather than lowercase and
#  underscores.
#  Constants are all upper case and prefixed with 'C_'
#  Function names are as for variables but prefixed with 'fn'
#  Local variables are prefixed with double underscores '__'
#
#===============================================================================
#                          C O N S T A N T S
#===============================================================================
#
readonly C_SCRIPTNAME=$(basename $0)   # get script name as a global
readonly C_TRUE="true"         # true/false test constant 
readonly C_FALSE="false"       # true/false test constant 
readonly C_KW1="@"             # keyword start character
readonly C_KW2="+"             # variable length keyword end character
readonly C_KW3="<"             # left justified fixed length keyword terminator
readonly C_KW4=">"             # right justified fixed length keyword terminator
readonly C_KW5=":"             # keyword/keyword data separator
readonly C_OV=".."             # set up overflow indicator string
readonly C_OL=${#C_OV}         # set up overflow indicator string length
#
#  error messages
#
readonly C_ERRNOARGS="$C_SCRIPTNAME - ERROR: No arguments provided\n"
readonly C_ERRBADPATH="$C_SCRIPTNAME - ERROR: %s is not a valid filepath\n"
#
#===============================================================================
#                            F U N C T I O N S
#===============================================================================
#
#-------------------------------------------------------------------------------
# fnFillBack [string] [separator] [terminator]
#     returns the original string with the characters following a separator
#     character replaced with the terminator character.
#
#     NB no validation of supplied parameters is undertaken. It is assumed that
#        the source string is not blank and that the 2nd and 3rd arguments are
#        single characters.
#
function fnFillBack() {
  local __String        # 1st argument - source string
  local __Len           # length of source string
  local __Sep           # 2nd argument - separator character
  local __Term          # 3rd argument - terminator character
  local __Result        # result place holder
  local __SepFound      # separator found flag
  __String="$1"         # get source string value
  __Len=${#__String}    # get source string length
  __Sep="$2"            # get separator value
  __Term="$3"           # get terminator value
  __SepFound="$C_FALSE" # unset separator found flag
  #
  # process source string one character at a time
  #
  for (( __i=0; __i<$__Len; __i++ )); do
    __Char="${__String:$__i:1}"            # get next character
    #
    # if the separator is found then switch on the flag
    #
    if [[ $__Char == $__Sep ]]; then
      __SepFound="$C_TRUE"
    fi
    #
    # if separator not found yet then replace the current character with the
    #  current character otherwise copy the terminator across
    #
    if [[ $__SepFound != "$C_TRUE" ]];then
      __Result="${__Result}${__Char}"
    else
      __Result="${__Result}${__Term}"    
    fi
  done
  #
  # return the result string
  #
  printf "%s" "$__Result"  
}
#
#-------------------------------------------------------------------------------
# fnSetup [command line arguments]
#     perform all the initialisation actions
#
function fnSetup() {
  #
  #  ensure that command line arguments are present
  #
  if [[ $# -eq 0 ]]; then
    printf "%s" "$C_ERRNOARGS"
    exit 1
  else
    #
    #  ensure that argument #1 is a resolvable absolute file name
    #  (rest of arguments ignored)
    #
    readonly C_SOURCEFILE=$(realpath -e $1)
    #
    #  test the exit value from the realpath command, if non zero then an error
    #  occured.
    #
    if [[ $? -gt 0 ]]; then
      # shellcheck disable=SC2059
      printf "$C_ERRBADPATH" "$1"
      exit 1
    fi
  fi
  #
  #  set up the keyword list as array
  #
  KeyWords=( "LastChangedDate"   "Date"                "LastChangedRevision"   \
             "Revision"          "Rev"                 "LastChangedBy"         \
             "Author"            "HeadURL"             "URL"                   \
             "Id"                "FileName"            "AuthorName"            \
             "AuthorEmail"       "AuthorDate"          "CommitLog"             \
             "CommitterName"     "CommitterEmail"      "CommitterDate"         \
             "RefNames"          "Subject"             "RawBody" )
  #
}
#===============================================================================
#                               M A I N
#===============================================================================
#
# fnMain() [command line arguments]
#   A main function is used as this script contains more than one other
#   function. It is the bottom most function so it is easy to find the start of
#   the program. This provides consistency with the rest of the code base as
#   well as allowing the definition of more variables as local (which can't be
#   done if the main code is not a function). The last non-comment line in the
#   file is a call to main.
#
function fnMain() {
  #
  # define local variables
  #
  local __Skip2Term="$C_FALSE" # skip to terminator (log history processing)
  #
  # invoke initialisation
  #
  fnSetup "$*"
  #
  #  read the source one line at a time from stdin
  #
  while IFS= read -r Sourceline; do
    #
    #  should we skip lines (presumably log lines)?
    #
    if [[ "$__Skip2Term" == "$C_TRUE" ]]; then
      #
      #  yes, then if a line ending with variable length keyword 
      #  end character is found...
      #
      if [[ "${Sourceline: -1}" == "$C_KW2" ]]; then
        #
        #  ...reset the skip lines flag
        #
        __Skip2Term="$C_FALSE"
      fi
      #
      #  finally, to skip this line, go back to the start of the loop
      #
      continue
    fi
    #
    #  no line skipping so iterate through the keyword list
    #
    for Keyword in ${KeyWords[@]}; do
      #
      #  test for presence of this keyword in current line
      #
      if [[ "$Sourceline" == *"${C_KW1}${Keyword}${C_KW5}"* ]]; then
        #
        # we know we have a line containing a keyword at this point, presumably
        # of the form "?[keyword]?[text]$" (or "?[keyword]??"for log history).
        #
        # test for variable length keyword data.
        #
        if   [[ ${Sourceline: -1} = "${C_KW2}" ]] ; then
          #
          # replace everything from and including the separator up to the end
          # of line with the saved terminator.
          #
          CleanedSource="${Sourceline/%${C_KW5}*/${C_KW2}}"
        #
        # test for fixed length left justified keyword data.
        #
        elif [[ ${Sourceline: -1} = "${C_KW3}" ]] ; then
          #
          # replace everything from and including the separator up to the end
          # of line with the saved terminator.
          #
          CleanedSource="$(fnFillBack "$Sourceline" "${C_KW5}" "${C_KW3}" )"
        #
        # test for fixed length right justified keyword data.
        #
        elif [[ ${Sourceline: -1} = "${C_KW4}" ]] ; then
          #
          # replace everything from and including the separator up to the end
          # of line with the saved terminator.
          #
          CleanedSource="$(fnFillBack "$Sourceline" "${C_KW5}" "${C_KW4}" )"
        #
        # if not one of the three expected terminators we assume this is the
        # start of a log history comment block.
        #
        else
          #
          # replace everything from and including the separator up to the end 
          # of line with the variable length data terminator.
          #
          CleanedSource="${Sourceline/%${C_KW5}*/${C_KW2}}"
          #
          # set flag to skip log lines
          #
          __Skip2Term="$C_TRUE"
        fi
        #
        #  replace the old source line with the updated (cleaned) line.
        #
        Sourceline="$CleanedSource"
      fi
    done
    #
    #  output the source line, use 'printf' instead of 'echo' to preserve
    #  formatting of line.
    #
    printf "%s\n" "$Sourceline"
  done
}
#
# invoke main function
#
fnMain "$*"
exit 0
