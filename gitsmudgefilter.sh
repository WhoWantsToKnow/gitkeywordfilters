#!/bin/bash
#===============================================================================
#  gitsmudgefilter.sh - git smudge filter written in bash to implement keyword
#  expansion
#===============================================================================
#
#  Copyright 2015 Keith Watson <swillber@gmail.com>
#
#  This file is part of the git keyword package and is free software; you can
#  redistribute it and/or modify it under the terms of the GNU General Public
#  License as published by the Free Software Foundation; either version 3 of the
#  License, or (at your option) any later version.
#
#  This package is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#  details.
#
#  You should have received a copy of the GNU General Public License along with
#  this package; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
#===============================================================================
#
#
#
#===============================================================================
#                          C O D I N G   S T Y L E
#===============================================================================
#  In general this script conforms to the Google Shell Style Guide (see:
#  http://google.github.io/styleguide/shell.xml).
#
#  Script uses CamelCaps for variable names rather than lowercase and
#  underscores.
#  Constants are all upper case and prefixed with 'C_'
#  Function names are as for variables but prefixed with 'fn'
#  Local variables are prefixed with double underscores '__'
#
#===============================================================================
#                          C O N S T A N T S
#===============================================================================
#
readonly C_KW1="@"             # keyword start character
readonly C_KW2="+"             # variable length keyword end character
readonly C_KW3="<"             # left justified fixed length keyword terminator
readonly C_KW4=">"             # right justified fixed length keyword terminator
readonly C_KW5=":"             # keyword/keyword data separator
readonly C_LOGHISTORY="LogHistory"  # log history keyword text
readonly C_VARIABLE="Variable" # variable length keyword text
readonly C_FIXLEFT="FixLeft"   # fixed length, left justified text
readonly C_FIXRIGHT="FixRight" # fixed length, right justified text
readonly C_OV=".."             # set up overflow indicator string
readonly C_OL=${#C_OV}         # set up overflow indicator string length
readonly C_NONE="None"         # test value for no keywords in source line
readonly C_SCRIPTNAME=$(basename "$0")
#
#  error messages
#
readonly C_ERRNOARGS="$C_SCRIPTNAME - ERROR: No arguments provided\n"
readonly C_ERRBADPATH="$C_SCRIPTNAME - ERROR: %s is not a valid filepath\n"
#
#===============================================================================
#                            F U N C T I O N S
#===============================================================================
#
#-------------------------------------------------------------------------------
# fnLogDate [%date_format]
#     Return date information in format dd-mmm-yyyy hh:mm:ss. Date information
#     to be retrieved is specified by the argument (see man git-log PRETTY
#     FORMATS format:<string> - placeholders - NOTE: this is not checked for
#     validity).
#
function fnLogDate() {
  #
  # use the git log command to extract date information for the latest revision.
  #
  local __DateTime=""
  __DateTime=$(git log -1 --format="$1" --date=rfc -- "$C_SOURCEFILE")
  #
  # use the date command to reformat the date time string and echo the
  # output as the function result.
  #
  printf "%s" "$(date -d "$__DateTime" "+%d-%b-%Y %H:%M:%S")"
}
#
#-------------------------------------------------------------------------------
# fnLogInfo [%specifier]
#     Return data from the latest revision. Information to be retrieved is
#     specified by the argument (see man git-log PRETTY FORMATS format:<string>
#     - placeholders).
#
function fnLogInfo() {
  #
  # use the git log command to extract data from the latest revision.
  #
  printf "%s" "$(git log -1 --format="$1" -- "$C_SOURCEFILE")"
}
#
#-------------------------------------------------------------------------------
# fnLastTag
#     Output the tag of the latest tagged commit in all branches
#     ref: http://stackoverflow.com/questions/1404796/ \
#                        how-to-get-the-latest-tag-name-in-current-branch-in-git
function fnLastTag(){
  printf "%s" "$(git describe --tags "$(git rev-list --tags --max-count=1)")"
}
#
#-------------------------------------------------------------------------------
# fnLtrim [string]
#     trim leading spaces from string argument
#     see http://stackoverflow.com/questions/369758/ \
#                                      how-to-trim-whitespace-from-bash-variable
function fnLtrim() {
  local __String="$*"
  __String="${__String#"${__String%%[![:space:]]*}"}"
  printf "%s\n" "$__String"
}
#
#-------------------------------------------------------------------------------
# fnRtrim [string]
#     trim trailing spaces from string argument
#     see http://stackoverflow.com/questions/369758/ \
#                                      how-to-trim-whitespace-from-bash-variable
function fnRtrim() {
  local __String="$*"
  __String="${__String%"${__String##*[![:space:]]}"}"
  printf "%s\n" "$__String"
}
#
#-------------------------------------------------------------------------------
# fnGetLength [keyword] [delimiter] [sourceline]
#     count number of characters between keyword and delimiter in source line.
#
function fnGetLength() {
  local __Count=0                    # result variable
  local __Keyword=$1                 # keyword
  # shellcheck disable=SC2034
  {
  local __Delim=$2                   # delimiter
  shift 2                            # drop 1st two arguments
  local __Sourceline="$*"            # get rest of arguments
  local __Substr="${__Sourceline%\$__Delim}" # trim text after delimiter
  }
  __Substr="${__Substr##*$__Keyword}" # trim text before keyword
  __Count=${#__Substr}               # get length of trimmed string
  printf "%u" "$__Count"             # return count
}
#
#-------------------------------------------------------------------------------
# fnLogHistory [sourceline]
#     replace the keyword with a comment list containing the reformatted log
#     data;
# e.g.
# $CommitLog:
#            Keith Watson 24-Aug-2015 13:05 Remove test README changes
#            Keith Watson 24-Aug-2015 13:01 Test README changes
#            Keith Watson 24-Aug-2015 12:58 Initial version
#            Keith Watson 24-Aug-2015 12:51 Initial version
# $
# takes a single argument which is the original source line containing the log
# history keyword.
#
function fnLogHistory() {
  #
  # define some local variables to process log data
  #
  local -a __Result  # result array
  local __Name       # log name field
  local __Date       # log date field
  local __Note       # log notes field
  local __Logout     # work string used to build output log line
  #
  # capture the sourceline argument into a local variable.
  #
  local __Sourceline="$*"
  #
  # output the log history keyword with the trailing delimiter replaced
  # with separator and overflow indicator string
  #
  __Result=("$(printf "%s\n" "${__Sourceline/${C_KW2}/${C_KW5}${C_OV}}")")
  #
  # reset the internal field separator to newline so we can load the output of
  # the git log command into an array
  #
  local __IFS=$IFS
  IFS=$'\n'
  #
  # use the git log command to extract a formatted list (author name, author
  # date, commit note subject line) of the revision history;
  # e.g.
  #    Keith Watson 2015-08-24 13:05:14 +0100 Remove test README changes
  #    Keith Watson 2015-08-24 13:01:59 +0100 Test README changes
  #    Keith Watson 2015-08-24 12:58:15 +0100 Initial version
  #    Keith Watson 2015-08-24 12:51:49 +0100 Initial version
  #
  # and convert this to an array.
  #
  local -a __history=( $(git log  -n -1 --pretty="%>|(25)%an %ai %s $2" \
                    -- "$C_SOURCEFILE") )
  #
  # reset field separator to original values
  #
  IFS="$__IFS"
  #
  # iterate over log output and extract the individual fields
  #
  for __LogLine in "${__history[@]}"; do
    local __StrWrk=""
    #
    # name field
    #
    __StrWrk=${__LogLine:0:25}
    __Name=$__StrWrk
    #
    # date field
    #
    __StrWrk=${__LogLine:26:26}
    __StrWrk=$(date -d "$__StrWrk" "+%d-%b-%Y %H:%M")
    __Date=$__StrWrk
    #
    # commit comment field
    #
    __StrWrk=${__LogLine:52}
    __StrWrk=$(fnRtrim "$__StrWrk")
    __Note=$__StrWrk
    #
    # build output data string
    #
    __Logout=$(printf "%s %s %s" "${__Name}" "${__Date}" "${__Note}")
    #
    # output log line by replacing keyword with output string to preserve
    # comment character(s)
    #
    __Result+=("$(printf "%s\n" "${__Sourceline/${C_KW1}*${C_KW2}/$__Logout}")")
  done
  #
  # output trailing log history string
  #
  __Result+=("$(printf "%s\n" "${__Sourceline/${C_KW1}*/${C_OV}${C_KW2}}")")
  #
  # return result
  #
  printf "%s\n" "${__Result[@]}"
}
#
#-------------------------------------------------------------------------------
# fnSetup [command line arguments]
#     perform all the initialisation actions
#
function fnSetup() {
  #
  # ensure that command line arguments are present
  #
  if [[ $# -eq 0 ]]; then
    printf "%s" "$C_ERRNOARGS"
    exit 1
  else
    #
    # ensure that argument #1 is a resolvable absolute file name
    # (rest of arguments ignored)
    #
    readonly C_SOURCEFILE=$(realpath -e "$1")
    if [[ $? -gt 0 ]]; then
      # shellcheck disable=SC2059
      printf "$C_ERRBADPATH" "$1"
      exit 1
    fi
  fi
  #
  # load the globally declared keyword array with the revision information
  #
  # shellcheck disable=SC2154
  {
  #
  # equivalent subversion keyword list
  #
  KeyWords["LastChangedDate"]="$(fnLogDate '%cd')"
  KeyWords["Date"]="$(fnLogDate '%cd')"
  KeyWords["LastChangedRevision"]="$(fnLastTag)"
  KeyWords["Revision"]="$(fnLastTag)"
  KeyWords["Rev"]="$(fnLastTag)"
  KeyWords["LastChangedBy"]="$(fnLogInfo '%cn')"
  KeyWords["Author"]="$(fnLogInfo '%cn')"
  KeyWords["HeadURL"]="$C_SOURCEFILE"
  KeyWords["URL"]="$C_SOURCEFILE"
  KeyWords["Id"]="$(basename "$C_SOURCEFILE") $(fnLastTag) $(fnLogDate '%cd') \
                  $(fnLogInfo '%cn')"
  #
  # new git keywords
  #
  KeyWords["FileName"]="$(basename "$C_SOURCEFILE")"
  KeyWords["AuthorName"]="$(fnLogInfo '%an')"
  KeyWords["AuthorEmail"]="$(fnLogInfo '%ae')"
  KeyWords["AuthorDate"]="$(fnLogDate '%ad')"
  KeyWords["CommitterName"]="$(fnLogInfo '%cn')"
  KeyWords["CommitterEmail"]="$(fnLogInfo '%ce')"
  KeyWords["CommitterDate"]="$(fnLogDate '%cd')"
  KeyWords["RefNames"]="$(fnLogInfo '%d')"
  KeyWords["Subject"]="$(fnLogInfo '%s')"
  KeyWords["RawBody"]="$(fnLogInfo '%B')"
  #
  # commit / log history list keywords - keyword text indicates log history
  # required
  #
  KeyWords["LogHistory"]=$C_LOGHISTORY
  KeyWords["CommitLog"]=$C_LOGHISTORY
  KeyWords["Log"]=$C_LOGHISTORY
  KeyWords["History"]=$C_LOGHISTORY
  }
}
#
#===============================================================================
#                                        M A I N
#===============================================================================
# fnMain() [command line arguments]
#   A main function is used as this script contains more than one other
#   function. It is the bottom most function so it is easy to find the start of
#   the program. This provides consistency with the rest of the code base as
#   well as allowing the definition of more variables as local (which can't be
#   done if the main code is not a function). The last non-comment line in the
#   file is a call to main.
#
function fnMain() {
  #
  # define local variables
  #
  local __Kwe             # work variable used to hold terminator character
  local __Keyfound        # flag used to indicate if a keyword has been found
  local __EreFl           # regex used to find left justified keywords
  local __EreFr           # regex used to find right justified keywords
  local __Keytext         # used to hold text associated with keyword
  local -a __LogHistory   # array used to hold the log history lines
  local __FixLen          # holds width of fixed text area
  local __TextLen         # actual length of keyword text
  local __Keyreplace      # holds replace string for keyword
  local __IFS=$IFS        # copy of IFS string
  #
  # invoke initialisation
  #
  fnSetup "$*"
  #
  # read the source line by line from stdin
  #
  while IFS= read -r Sourceline; do
    #
    # clear flag used to indicate keyword found
    #
    __Keyfound=$C_NONE
    #
    # iterate through the keyword list
    #
    for __Keyword in "${!KeyWords[@]}"; do
      #
      # build extended regular expression to match a fixed width keyword with
      # the keyword text left justified
      #
      __EreFl="^.*[${C_KW1}]${__Keyword}[${C_KW3}]+.*$"
      #
      # build extended regular expression to match a fixed width keyword with
      # the keyword text right justified
      #
      __EreFr="^.*[${C_KW1}]${__Keyword}[${C_KW4}]+.*$"
      #
      # flag if keyword (variable length) is in current line and exit loop
      #
      if [[ $Sourceline == *${C_KW1}${__Keyword}${C_KW2}*  ]]; then
        __Keyfound=$C_VARIABLE
        __Kwe=$C_KW2
        break
      #
      # flag if keyword (fixed left justified) is in current line and exit loop
      #
      elif [[ $Sourceline =~ $__EreFl ]]; then
        __Keyfound=$C_FIXLEFT
        __Kwe=$C_KW3
        break
      #
      # flag if keyword (fixed right justified) is in current line and exit loop
      #
      elif [[ $Sourceline =~ $__EreFr ]]; then
        __Keyfound=$C_FIXRIGHT
        __Kwe=$C_KW4
        break
      fi
    done
    #
    # keyword found
    #
    if [[ $__Keyfound != $C_NONE ]]; then
      #
      # get keyword text
      #
      __Keytext="${KeyWords[$__Keyword]}"
      #
      # as log history has multiline output it is processed separately from the
      # other keywords
      #
      if [[ $__Keytext = "$C_LOGHISTORY" ]]; then
        #
        # clear down the array used to hold the log history lines
        #
        unset __LogHistory
        #
        # get the log history lines
        #
        __LogHistory=$(fnLogHistory "$Sourceline")
        #
        # convert array into a string with newline delimiters between elements
        #
        printf -v Sourceline "%s\n" "${__LogHistory[@]}"
        #
        # remove last character of string (unwanted newline)
        #
        Sourceline="${Sourceline%?}"
      else
        #
        # process fixed length output
        #
        if [[ $__Keyfound = "$C_FIXLEFT" || $__Keyfound = "$C_FIXRIGHT" ]]; then
          #
          # count number of characters between keyword and final character
          # - i.e. width of fixed text area
          #
          __FixLen=$(fnGetLength "$__Keyword" "$__Kwe" "$Sourceline")
          #
          # get actual length of keyword text
          #
          __TextLen=${#__Keytext}
          #
          # if keyword text longer than fixed text area
          #
          if  (("$__TextLen" > "$__FixLen")); then
            #
            # trim keyword length to match the fixed text
            #
            if [[ $__Keyfound = "$C_FIXLEFT" ]]; then
              # left justified - trim from end and add terminator to end
              __Keytext="${__Keytext:0:(($__FixLen-$C_OL))}${C_OV}"
            else
              # right justified - trim from front and add terminator to front
              __Keytext="${C_OV}${__Keytext:$((__TextLen-__FixLen+C_OL))}"
            fi
          #
          # keyword text length less than fixed text area
          #
          else
            #
            # so pad to length of fixed area
            #
            if [[ $__Keyfound = "$C_FIXLEFT" ]]; then
              # left justified
              __Keytext=$(printf "%-*.*s" "$__FixLen" "$__FixLen" "$__Keytext")
            else
              # right justified
              __Keytext=$(printf "%*.*s" "$__FixLen" "$__FixLen" "$__Keytext")
            fi
          fi
        fi
        #
        # set up replace string for keyword
        #
        __Keyreplace="${C_KW1}${__Keyword}${C_KW5}${__Keytext}${__Kwe}"
        #
        # replace keyword with keyword+text
        #
        Sourceline="${Sourceline/${C_KW1}*${__Kwe}/$__Keyreplace}"
      fi
    fi
    #
    # output current source line to stdout
    #
    printf "%s\n" "$Sourceline"
  done
  exit 0
}
#
# set up keyword list as a global associative array with the array values as the
# keyword text
#
declare -A KeyWords
#
# invoke main function
#
fnMain "$*"
